Switch component then declares with Route we can go to.
	-- /courses is visited in the browser. Reactjs will show the Courses Component

The "as" prop allows components to be treated as if they are a different component
The "to" prop is used in place of "href" prop for providiing the URL for the page
The "exact" prop is used to highlight the active NavLink component that matches the URL


======================
Activity
======================
1. Create an "Error" page that will rendered whenever an undefined route is accessed.
2. Import the "Error" page and add the route.
3. Refactor the "Banner" component to receive a "data" prop to be reusable on both the 
"Error" and "Home" page.
4. Add the "data" for the "Banner" component in the "Home" page to show the previously used 
information.