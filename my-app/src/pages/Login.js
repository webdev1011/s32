import React, { useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Login() {
	const { user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);



	function authenticate(e) {
		e.preventDefault();

		setEmail('');
		setPassword('');

        //set the email of the authentiated user in the local storage

		alert(`${email} has been verified! Welcome back!`);

	}
	useEffect(() => {

        // Validation to enable submit button when all fields are populated
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (
        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )
}