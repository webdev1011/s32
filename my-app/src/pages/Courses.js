import React, { Fragment } from 'react';
import Course from '../components/Course';
import courseData from '../data/courses';

export default function Courses(){
	const courses = courseData.map(course => {
		return (
			<Course key={course.id} course={course} />
		);
	})

	return (
		<Fragment>
			{courses}
		</Fragment>
	)

}