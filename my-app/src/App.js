import React, { Fragment, useState } from 'react';
import { Container } from 'react-bootstrap';
import Navbar from './components/Navbar';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { UserProvider } from './UserContext';

//page components
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

function App() {
	//State hook for the user state that's defined here for a global scope
	//Initialized as an object with properties from the localstorage
	const [user, setUser] = useState({
		email: localStorage.getItem('email'),

		//Data stored in localStorae is converted into string. Added condition
		isAdmin: localStorage.getItem('isAdmin') === 'true'

	})

	//Function for clearing localStorage on logout
	const unsetUser = () => {
		localStorage.clear();

		//Changes the value of the user state back to it's original value
		setUser({
			email: null,
			isAdmin: null
		});
	}
		return (
			<Fragment>
				<UserProvider value={{user, setUser, unsetUser}}>
					<Router>
						<Navbar />
						<Container>
							<Switch>
								<Route exact path="/" component={Home} />
								<Route exact path="/courses" component={Courses} />
								<Route exact path="/logout" component={Logout} />
								<Route exact path="/login" component={Login} />
								<Route exact path="/register" component={Register} />
								<Route component={Error} />
							</Switch>
						</Container>
					</Router> 
				</UserProvider>     
			</Fragment>
			
		);
}

export default App;
